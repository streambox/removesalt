sort_yaml:
	yq -i 'sort_keys(..)' src/removesalt/conf/config.yaml
	yq -i 'sort_keys(..)' src/removesalt/conf/app/iris.yaml
	yq -i 'sort_keys(..)' src/removesalt/conf/app/mediaplayer.yaml
	yq -i 'sort_keys(..)' src/removesalt/conf/app/spectra.yaml
	yq -i 'sort_keys(..)' src/removesalt/conf/app/spectra-cdi.yaml
	yq -i 'sort_keys(..)' src/removesalt/conf/app/base.yaml
	yq -i 'sort_keys(..)' src/removesalt/conf/pipeline/appveyor.yaml
	yq -i 'sort_keys(..)' src/removesalt/conf/pipeline/gitlab.yaml
	yq -i 'sort_keys(..)' src/removesalt/conf/pipeline/base.yaml
