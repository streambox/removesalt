import abc
import inspect
import logging
import pathlib
import shutil
import subprocess

import giftmaster
import omegaconf
import pyscaffold.shell

from . import helper, s3, sign, template

log = logging.getLogger(__name__)


def run(cmdobj):
    log.debug(cmdobj.name)

    dry_run = cmdobj.cfg.app.wix.dry_run
    cmd = pyscaffold.shell.ShellCommand(cmdobj.cmd, shell=dry_run)

    try:
        cmd(*cmdobj.args)
    except subprocess.CalledProcessError as e:
        if e.returncode in cmdobj.acceptable_returncodes:
            return
        raise e


class BuildGuideline(abc.ABC):
    def build(self, cfg: omegaconf.DictConfig):
        self.report_s3_endpoints(cfg)
        self.setup_common(cfg)
        self.gather_binaries(cfg)
        if cfg.pipeline.run_signtool == "signed":
            self.sign(cfg)
        self.fill_templates(cfg)
        self.wix_heat(cfg)  # create .wxs.j2 from heat dir harvest
        self.wix_candle(cfg)  # create .wixobj from .wxs.j2
        self.wix_light(cfg)  # create .msi from .wixobj
        if cfg.pipeline.run_signtool == "signed":
            self.sign(cfg)
        self.wix_bundle(cfg)  # create bundle .exe from .msi
        self.wix_light2(cfg)  # output bundle.exe
        if cfg.pipeline.run_signtool == "signed":
            self.sign(cfg)
            self.wix_unsign_bundle(cfg)
            self.wix_insignia_deatch(cfg)
            self.sign(cfg)
            self.wix_insignia_reattach(cfg)
            self.sign(cfg)
        self.layout_s3(cfg)
        self.s3sync(cfg)

    @abc.abstractmethod
    def gather_binaries(self, cfg: omegaconf.DictConfig):
        pass

    def report_s3_endpoints(self, cfg: omegaconf.DictConfig):
        log.debug(cfg.app.artifacts.url.versioned)
        log.debug(cfg.app.artifacts.url.latest)

    def setup_common(self, cfg: omegaconf.DictConfig):
        cfg.app.deploy.mkdir(parents=True, exist_ok=True)
        cfg.app.work1.mkdir(parents=True, exist_ok=True)

        shutil.copy(cfg.app.icon.path_source, cfg.app.icon.path_work)
        shutil.copy(cfg.app.splash_image.path_source, cfg.app.splash_image.path_work)
        cfg.app.ver_file.path_work.write_text(cfg.app.app.version)

    @abc.abstractmethod
    def layout_s3(self, cfg: omegaconf.DictConfig):
        pass

    def fill_templates(self, cfg: omegaconf.DictConfig):
        template.main(cfg)

    def wix_heat(self, cfg: omegaconf.DictConfig) -> None:
        transform = """<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:msxsl="urn:schemas-microsoft-com:xslt"
                xmlns:wix="http://schemas.microsoft.com/wix/2006/wi"
                exclude-result-prefixes="msxsl wix">
  <xsl:output method="xml" indent="yes" />

  <xsl:template match="node()|@*">
    <xsl:copy>
      <xsl:apply-templates select="node()|@*"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="//*[local-name()='Component']">
    <wix:Component Id="{@Id}" Directory="{@Directory}" Guid="{@Guid}">
      <xsl:if test="contains(*[local-name()='File']/@Source, 'settings.xml') or contains(*[local-name()='File']/@Source, 'encoder-preference.txt')"> <!-- # noqa: E501: hey flake8! please allow wide lines -->
        <xsl:attribute name="Permanent">yes</xsl:attribute>
      </xsl:if>
      <xsl:apply-templates select="@* | node()"/>
    </wix:Component>
  </xsl:template>

  <xsl:template match="@KeyPath">
    <xsl:choose>
      <xsl:when test="contains(parent::node()/@Source, 'settings.xml') or contains(parent::node()/@Source, 'encoder-preference.txt')">
        <xsl:attribute name="KeyPath">
          <xsl:value-of select="'yes'"/>
        </xsl:attribute>
      </xsl:when>
    </xsl:choose>
  </xsl:template>
</xsl:stylesheet>
        """

        for name in cfg.app.components.build_order:
            component = cfg.app.components.container[name]

            transform_path = component.path_work.parent / "transform.xslt"
            transform_path.write_text(transform)

            if not component.single_file:
                args = [
                    "dir",
                    str(component.deploy),
                    "-out",
                    str(component.path_work),
                    "-cg",
                    component.group,
                    "-t",
                    str(transform_path),
                    "-dr",
                    component.root_dir,
                    "-var",
                    "var.SourceFilesDir",
                    "-ag",
                    "-g1",
                    "-srd",
                    "-sreg",
                    "-arch",
                    "x64",
                    "-nologo",
                ]

                fcn_name = inspect.currentframe().f_code.co_name
                sc = helper.ShellCommand(fcn_name, "heat", args, cfg)
                run(sc)

    def wix_candle(self, cfg: omegaconf.DictConfig) -> None:
        for name in cfg.app.components.build_order:
            component = cfg.app.components.container[name]

            args = [
                "-out",
                str(component.path_work.with_suffix(".wixobj")),
                str(component.path_work),
            ]

            args.extend(
                [
                    f"-dSourceFilesDir={str(component.deploy)}",
                ]
            )

            args.extend(
                [
                    "-ext",
                    "WixUtilExtension",
                    "-arch",
                    "x64",
                    "-nologo",
                ]
            )

            fcn_name = inspect.currentframe().f_code.co_name
            sc = helper.ShellCommand(fcn_name, "candle", args, cfg)
            run(sc)

    def wix_light(self, cfg: omegaconf.DictConfig) -> None:
        wixobj_paths = []
        for name in cfg.app.components.build_order:
            component = cfg.app.components.container[name]
            path = component.path_work.with_suffix(".wixobj")
            wixobj_paths.append(str(path))

        args = [
            "-out",
            str(cfg.app.msi.path_work),
            *wixobj_paths,
            "-ext",
            "WixUtilExtension",
            "-ext",
            "WixUIExtension",
            "-spdb",
            "-nologo",
            "-sice:ICE36",
            "-sice:ICE57",
        ]

        fcn_name = inspect.currentframe().f_code.co_name
        sc = helper.ShellCommand(
            fcn_name, "light", args, cfg, acceptable_returncodes=[0, 204]
        )
        run(sc)

    def wix_bundle(self, cfg: omegaconf.DictConfig) -> None:
        args = [
            "-out",
            str(cfg.app.bundle.wix_source.work.with_suffix(".wixobj")),
            str(cfg.app.bundle.wix_source.work),
            f"-dMsiPath={str(cfg.app.msi.path_work)}",
            f"-dThemeFile={str(cfg.app.theme_file.path_work)}",
            f"-dLocalizationFile={str(cfg.app.localization_file.path_work)}",
            "-ext",
            "WixBalExtension",
            "-ext",
            "WixUtilExtension",
            "-arch",
            "x64",
            "-nologo",
        ]

        fcn_name = inspect.currentframe().f_code.co_name
        sc = helper.ShellCommand(fcn_name, "candle", args, cfg)
        run(sc)

    def wix_light2(self, cfg: omegaconf.DictConfig) -> None:
        args = [
            "-out",
            str(cfg.app.bundle.bin),
            str(cfg.app.bundle.wix_source.work.with_suffix(".wixobj")),
            f"-dMsiPath={str(cfg.app.msi.path_work)}",
            f"-dThemeFile={str(cfg.app.theme_file.path_work)}",
            f"-dLocalizationFile={str(cfg.app.localization_file.path_work)}",
            "-ext",
            "WixBalExtension",
            "-spdb",
            "-nologo",
        ]

        fcn_name = inspect.currentframe().f_code.co_name
        sc = helper.ShellCommand(fcn_name, "light", args, cfg)
        run(sc)

    def wix_insignia_deatch(self, cfg: omegaconf.DictConfig) -> None:
        args = [
            "-ib",
            str(cfg.app.bundle.bin),
            "-o",
            str(cfg.app.engine),
        ]

        fcn_name = inspect.currentframe().f_code.co_name
        sc = helper.ShellCommand(fcn_name, "insignia", args, cfg)
        run(sc)

    def wix_insignia_reattach(self, cfg: omegaconf.DictConfig) -> None:
        args = [
            "-ab",
            str(cfg.app.engine),
            str(cfg.app.bundle.bin),
            "-o",
            str(cfg.app.bundle.bin),
        ]

        fcn_name = inspect.currentframe().f_code.co_name
        sc = helper.ShellCommand(fcn_name, "insignia", args, cfg)
        run(sc)

    def sign(self, cfg: omegaconf.DictConfig) -> None:
        sign.sign_files(cfg.app.work)

    def wix_unsign_bundle(self, cfg: omegaconf.DictConfig) -> None:
        # signtool remove /s bundle.exe
        cmd_list = giftmaster.signtool.unsign_cmd(cfg.app.bundle.bin)
        s1 = cmd_list.pop(0)
        s2 = pathlib.Path(s1).name
        s3 = pathlib.Path(s2).stem
        name = s3
        fcn_name = inspect.currentframe().f_code.co_name
        sc = helper.ShellCommand(fcn_name, name, cmd_list, cfg)
        run(sc)

    def s3sync(self, cfg: omegaconf.DictConfig):
        s3.sync_to_s3(cfg)
