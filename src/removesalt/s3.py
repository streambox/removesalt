import inspect
import logging
import shlex
import subprocess

from pyscaffold import shell as pyshell

from . import helper

log = logging.getLogger(__name__)


def sync_to_s3(cfg):
    cmd_sync = [
        "aws",
        "s3",
        "sync",
        str(cfg.app.s3bucket.path_work),
        cfg.app.s3bucket.endpoint,
        "--region",
        "us-west-2",
        "--no-progress",
        "--acl",
        "public-read",
    ]

    cfg.app.s3bucket.sync_script.write_text(shlex.join(cmd_sync))

    if cfg.pipeline.stop_all_uploads:
        log.info("skipping sync to s3 because pipeline.stop_all_uploads=true")
        return

    fcn_name = inspect.currentframe().f_code.co_name
    sc = helper.ShellCommand(
        fcn_name, cmd="powershell", args=[str(cfg.app.s3bucket.sync_script)], cfg=cfg
    )

    dry_run = sc.cfg.app.wix.dry_run
    aws_sync = pyshell.ShellCommand(sc.cmd, shell=dry_run)

    log.debug(sc.name)
    try:
        aws_sync(*sc.args)
    except subprocess.CalledProcessError as e:
        if e.returncode in sc.acceptable_returncodes:
            return
        raise e
