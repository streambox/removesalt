import pathlib

import hydra
import omegaconf


def initialize_resolvers():
    omegaconf.OmegaConf.register_new_resolver("lower", lambda x: str(x).lower())
    omegaconf.OmegaConf.register_new_resolver("path", pathlib.Path)
    omegaconf.OmegaConf.register_new_resolver(
        "orig_cwd", lambda: pathlib.Path(hydra.utils.get_original_cwd())
    )
