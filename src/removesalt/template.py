import logging
import pathlib
import shutil

import jinja2
import pkg_resources

log = logging.getLogger(__name__)


def get_templates_path():
    package = __name__.split(".")[0]
    TEMPLATES_PATH = pathlib.Path(pkg_resources.resource_filename(package, "templates"))
    return TEMPLATES_PATH


def main(cfg):
    for thing in [
        cfg.app.app,
        cfg.app.bundle,
    ]:
        dir_paths = [
            get_templates_path() / cfg.app.wix.templates_dir,
            get_templates_path() / "partials",
        ]

        loader = jinja2.FileSystemLoader(dir_paths)
        env = jinja2.Environment(loader=loader, keep_trailing_newline=True)
        template = env.get_template(thing.wix_source.template.name)
        rendered = template.render(data=cfg.app)
        log.debug(f"writing to {thing.wix_source.work}")
        thing.wix_source.work.write_text(rendered)

    shutil.copy(
        get_templates_path() / cfg.app.theme_file.name,
        cfg.app.theme_file.path_work,
    )
    shutil.copy(
        get_templates_path() / cfg.app.localization_file.name,
        cfg.app.localization_file.path_work,
    )
