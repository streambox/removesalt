import logging
import pathlib

import hydra
import omegaconf
import yaml

from . import util

_logger = logging.getLogger(__name__)


"""
python -m removesalt.main_test +app=spectra hydra/hydra_logging=disabled # noqa: E501
python -m removesalt.main_test +app=spectra-cdi hydra/hydra_logging=disabled # noqa: E501

diff -uw <(python -m removesalt.main_test +app=spectra hydra/hydra_logging=disabled) <(python -m removesalt.main_test +app=spectra-cdi hydra/hydra_logging=disabled)  # noqa: E501
"""


def path_representer(dumper, data):
    return dumper.represent_scalar("tag:yaml.org,2002:str", str(data))


@hydra.main(version_base=None, config_path="conf", config_name="config")
def my_app(cfg: omegaconf.DictConfig) -> None:
    util.initialize_resolvers()
    omegaconf.OmegaConf.resolve(cfg)
    # yaml.add_representer(pathlib.PosixPath, path_representer)
    yaml.add_multi_representer(pathlib.PurePath, path_representer)

    # workaround to expand yaml aliases and anchors:
    myyaml = omegaconf.OmegaConf.to_yaml(cfg)
    dct = yaml.safe_load(myyaml)
    cfg_as_yaml = yaml.dump(dct)

    print(cfg_as_yaml)


if __name__ == "__main__":
    _logger.info("Starting script...")
    my_app()
