import argparse
import distutils.dir_util
import distutils.file_util
import logging
import shutil
import zipfile

import omegaconf

from removesalt import build_guideline, template

log = logging.getLogger(__name__)


def setup(cfg):
    appdir = cfg.app.bin_path / "app"

    files = [
        appdir / x
        for x in [
            "decoder.exe",
            "controlpanel.exe",
            "encassist.ini",
            "chromactivate.exe",
            "sb-hd-bmp.bmp",
        ]
    ]
    for file in files:
        shutil.copy(file, cfg.app.deploy)

    files = [
        cfg.app.installer_source_path / x
        for x in [
            "nssm.exe",
            "service.ps1",
            "cleanlogs.ps1",
            "firewall.ps1",
        ]
    ]
    for file in files:
        log.debug(f"copy {file.resolve()} to {cfg.app.deploy.resolve()}")
        shutil.copy(file, cfg.app.deploy)

    curl = cfg.app.deploy / "curl"
    curl.mkdir(exist_ok=True, parents=True)
    files = [
        appdir / "curl" / x
        for x in [
            "curl.exe",
            "libeay32.dll",
            "libssl32.dll",
            "msvcr90.dll",
        ]
    ]
    for file in files:
        shutil.copy(file, curl)

    log.debug(
        f"writing version {cfg.app.app.version} to {cfg.app.ver_file.path_deploy}"
    )
    cfg.app.ver_file.path_deploy.write_text(cfg.app.app.version)

    templates_path = template.get_templates_path()
    p1 = templates_path / "iris" / "custom_actions.wxs.j2"
    shutil.copy(p1, cfg.app.components.container.actions.path_work)


class Iris(build_guideline.BuildGuideline):
    def gather_binaries(self, cfg: omegaconf.DictConfig):
        setup(cfg)

    def layout_s3(self, cfg: omegaconf.DictConfig):
        # Example
        # win/0.5.1.1/version.txt
        # win/0.5.1.1/streambox_iris_win.zip
        bucket_ver_dir = cfg.app.s3bucket.path_versioned
        bucket_ver_dir.mkdir(exist_ok=True, parents=True)

        zip1 = cfg.app.bundle.bin.parent / cfg.app.zip_not_versioned.name
        z1 = cfg.app.bundle.bin.parent / cfg.app.zip_versioned.path.name
        zip_path = f"{z1.stem}/{cfg.app.bundle.bin.name}"

        with zipfile.ZipFile(zip1, mode="w") as archive:
            log.debug(f"zip/append {cfg.app.bundle.bin} to {zip1}")
            archive.write(cfg.app.bundle.bin.resolve(), zip_path)

        shutil.copy(zip1, cfg.app.s3bucket.zip_not_versioned)
        shutil.copy(zip1, bucket_ver_dir)
        shutil.copy(cfg.app.ver_file.path_work, bucket_ver_dir)

        # Guides
        cfg.app.s3bucket.path_latest.mkdir(exist_ok=True, parents=True)
        guides = cfg.app.bin_path / "docs/s3"
        dst = cfg.app.s3bucket.path_latest
        distutils.dir_util.copy_tree(str(guides), str(dst))


def main(cfg):
    setup(cfg)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    args = parser.parse_args()
    main(args)
