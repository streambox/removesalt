import argparse
import distutils.dir_util
import distutils.file_util
import logging
import shutil
import zipfile

import omegaconf
import pyscaffold.shell
import saltgang
import saltgang.encassist
import saltgang.settings

from removesalt import build_guideline, template

log = logging.getLogger(__name__)


class Spectra(build_guideline.BuildGuideline):
    def gather_binaries(self, cfg: omegaconf.DictConfig):
        setup(cfg)

    def layout_s3(self, cfg: omegaconf.DictConfig):
        s3basedir = cfg.app.s3bucket.path_versioned
        s3basedir.mkdir(exist_ok=True, parents=True)

        with zipfile.ZipFile(cfg.app.zip_versioned.path_work, mode="w") as archive:
            archive.write(
                cfg.app.bundle.bin,
                f"{cfg.app.zip_versioned.path_work.stem}/{cfg.app.bundle.bin.name}",
            )

        shutil.copy(cfg.app.zip_versioned.path_work, s3basedir)

        # latest/win/version.txt
        shutil.copy(cfg.app.ver_file.path_work, s3basedir)

        # overwrite spectra_avid_win.exe with spectra_universal_win.exe
        artifacts_legacy = [
            # latest/win/adobe/spectra.exe
            s3basedir / "adobe" / "spectra.exe",
            # latest/win/avid/spectra.exe
            # latest/win/avid/spectra_avid_win.exe
            s3basedir / "avid" / "spectra.exe",
            s3basedir / "avid" / "spectra_avid_win.exe",
        ]

        # overwrite spectra_avid_win.exe with spectra_universal_win.exe
        artifacts_universal = [
            # latest/win/universal/spectra.exe
            # latest/win/universal/spectra_universal_win.exe
            s3basedir / "universal" / "spectra.exe",
            s3basedir / "universal" / "spectra_universal_win.exe",
        ]

        artifacts_universal_and_legacy = artifacts_legacy + artifacts_universal
        artifacts_cdi = [cfg.app.s3bucket.bundle_path]

        # FIXME: SHOOT ME PLEASE
        artifacts = None
        if cfg.app.sku == "universal":
            artifacts = artifacts_universal_and_legacy
            # legacy latest/win/spectra.zip
            shutil.copy(cfg.app.zip_versioned.path_work, s3basedir / "spectra.zip")

        shutil.copy(cfg.app.zip_versioned.path_work, cfg.app.s3bucket.zip_not_versioned)

        if cfg.app.sku.lower() == "cdi":
            artifacts = artifacts_cdi

        for path in artifacts:
            path.parent.mkdir(exist_ok=True, parents=True)
            shutil.copy(cfg.app.bundle.bin, path)

        # FIXME: not handled: latest/spectra_win.zip

        # Guides
        # latest/win/adobe/quickstart.pdf
        # latest/win/avid/quickstart.pdf
        # latest/win/avid/spectra_quickstart_win_avid.pdf
        # latest/win/universal/quickstart.pdf
        # latest/win/universal/spectra_quickstart_win_universal.pdf
        cfg.app.s3bucket.path_latest.mkdir(exist_ok=True, parents=True)
        guides = cfg.app.bin_path / "docs/s3"
        dst = cfg.app.s3bucket.path_latest
        distutils.dir_util.copy_tree(str(guides), str(dst))


def generate_encassist_ini(cfg):
    parser = argparse.ArgumentParser()
    saltgang.settings.add_arguments(parser)
    args = parser.parse_args(
        [
            "--outpath",
            str(cfg.app.spectra.encassist.ini.deploy),
            "--yaml-path",
            str(cfg.app.spectra.encassist.yml.work_path),
            "--ini",
        ]
    )
    saltgang.settings.main(args)


def generate_encassist_yml(cfg):
    parser = argparse.ArgumentParser()
    saltgang.encassist.add_arguments(parser)
    args = parser.parse_args(
        [
            "--outpath",
            str(cfg.app.spectra.encassist.yml.work_path),
            "--config-basedir",
            str(cfg.app.spectra.encassist.gitclone),
            "--sku",
            cfg.app.sku,
            "--force-overwrite-conf",
        ]
    )
    saltgang.encassist.main(args)


def generate_set_defaults_exe(cfg):
    parser = argparse.ArgumentParser()
    saltgang.settings.add_arguments(parser)
    args = parser.parse_args(
        [
            "--outpath",
            str(cfg.app.spectra.encassist.set_defaults.src_path),
            "--yaml-path",
            str(cfg.app.spectra.encassist.yml.work_path),
            "--go",
        ]
    )
    # create set_defaults.go
    saltgang.settings.main(args)

    # go build set_defaults.exe
    paths = [
        cfg.app.bin_path / "installer/set_defaults/go.mod",
        cfg.app.bin_path / "installer/set_defaults/go.sum",
    ]
    for path in paths:
        dst = cfg.app.work1 / path.name
        distutils.file_util.copy_file(str(path), str(dst))

    go = pyscaffold.shell.ShellCommand("go")

    output = go("fmt", cfg.app.spectra.encassist.set_defaults.src_path)
    log.debug(list(output))

    output = go(
        "build",
        cfg.app.spectra.encassist.set_defaults.src_path,
        cwd=cfg.app.spectra.encassist.set_defaults.src_path.parent,
    )
    log.debug(list(output))

    dst = cfg.app.spectra.encassist.set_defaults.deploy_path
    src = cfg.app.spectra.encassist.set_defaults.bin_path
    distutils.file_util.copy_file(str(src), str(dst))


def setup(cfg):
    # FIXME: do this properly
    if cfg.app.sku not in ["universal", "cdi"]:
        msg = f"I didn't expect to see {cfg.app.sku}"
        log.critical(msg)
        raise ValueError(msg)

    # component: C:\Program Files\Avid\AVX2_Plug-ins\Spectra.acf
    src = cfg.app.bin_path / "avid"
    dst = cfg.app.deploy2
    distutils.dir_util.copy_tree(str(src), str(dst))

    # component: C:\Program Files\Adobe\Common\Plug-ins\7.0\MediaCore\Spectra.prm
    src = cfg.app.bin_path / "adobe"
    dst = cfg.app.deploy3
    distutils.dir_util.copy_tree(str(src), str(dst))

    # component: C:\ProgramData\Streambox\SpectraUI
    src = cfg.app.bin_path / "spectraui"
    dst = cfg.app.deploy4
    distutils.dir_util.copy_tree(str(src), str(dst))
    cfg.app.ver_file.path_deploy.write_text(cfg.app.app.version)
    (cfg.app.ver_file.path_deploy.parent / "hash/.gitkeep").unlink(missing_ok=True)
    (cfg.app.ver_file.path_deploy.parent / "log/.gitkeep").unlink(missing_ok=True)
    (cfg.app.ver_file.path_deploy.parent / "log/encoder.log").touch()

    # component: C:\Program Files\Common Files\OFX\Plugins\SpectraPlugin.ofx.bundle\Contents\Win64\SpectraPlugin.ofx # noqa: E501
    src = cfg.app.bin_path / "ofx"
    dst = cfg.app.deploy5
    distutils.dir_util.copy_tree(str(src), str(dst))

    # component: C:\UE_5.1\Engine\Plugins\Media\StreamboxSpectra
    src = cfg.app.bin_path / "app-unreal"
    dst = cfg.app.deploy1
    distutils.dir_util.copy_tree(str(src), str(dst))

    # component: C:\Program Files\Streambox\Spectra
    for app_bin_dir in cfg.app.app_bin_component_dirs:
        src = cfg.app.bin_path / app_bin_dir
        dst = cfg.app.deploy
        distutils.dir_util.copy_tree(str(src), str(dst))

    generate_encassist_yml(cfg)  # generate encassist.yml and then encassist.ini
    generate_encassist_ini(cfg)  # don't run until dist folder exists

    templates_path = template.get_templates_path()
    act = templates_path / "spectra" / "custom_actions.wxs.j2"
    shutil.copy(act, cfg.app.components.container.actions.path_work)

    generate_encassist_yml(cfg)
    generate_set_defaults_exe(cfg)


def main(cfg):
    setup(cfg)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    args = parser.parse_args()
    main(args)
