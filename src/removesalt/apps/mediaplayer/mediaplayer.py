import argparse
import distutils.dir_util
import distutils.file_util
import logging
import shutil
import zipfile

import omegaconf

from removesalt import build_guideline, template

log = logging.getLogger(__name__)


def setup(cfg):
    file_list = [
        "SMPlayer.exe",
        "SMPlayerLib.dll",
        "Microsoft.Expression.Interactions.dll",
        "Microsoft.Wpf.Interop.DirectX.dll",
        "Microsoft.Xaml.Behaviors.dll",
        "System.Windows.Interactivity.dll",
        "chroma2k35.bmp",
        "dxLib.dll",
    ]

    for fname in file_list:
        src = cfg.app.bin_path / "app" / fname
        dst = cfg.app.deploy / src.name
        log.debug(f"copying {src} to {dst}")
        shutil.copy(src, dst)

    src = cfg.app.bin_path / "installer" / "firewall.ps1"
    dst = cfg.app.deploy / src.name
    log.debug(f"copying {src} to {dst}")
    shutil.copy(src, dst)

    templates_path = template.get_templates_path()
    p1 = templates_path / "mediaplayer" / "custom_actions.wxs.j2"
    shutil.copy(p1, cfg.app.components.container.actions.path_work)
    # workaround for https://basecamp.com/2498935/projects/11886956/todos/465658688#comment_871862482 # noqa: E501
    workaround_version = cfg.app.app.version.replace("1.20.0.0", "1.20")
    cfg.app.ver_file.path_deploy.write_text(workaround_version)


def main(cfg):
    setup(cfg)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    args = parser.parse_args()
    main(args)


class Mediaplayer(build_guideline.BuildGuideline):
    def gather_binaries(self, cfg: omegaconf.DictConfig):
        setup(cfg)

    def layout_s3(self, cfg: omegaconf.DictConfig):
        # Create artifacts:
        # win/1.12.0.1/mediaplayer.zip
        # win/1.12.0.1/mediaplayer_1.12.0.1.zip
        # win/1.12.0.1/version.txt
        bucket_ver_dir = cfg.app.s3bucket.path_versioned
        bucket_ver_dir.mkdir(exist_ok=True, parents=True)

        # win/1.12.0.1/mediaplayer_1.12.0.1.zip
        z1 = cfg.app.bundle.bin.parent / cfg.app.zip_versioned.path.name
        zip_path = f"{z1.stem}/{cfg.app.bundle.bin.name}"
        with zipfile.ZipFile(z1, mode="w") as archive:
            archive.write(cfg.app.bundle.bin.resolve(), zip_path)

        # copy from work dir to bucket stage dir
        shutil.copy(z1, cfg.app.zip_versioned.path)

        # win/1.12.0.1/mediaplayer.zip
        shutil.copy(z1, cfg.app.zip_not_versioned.path)

        # win/1.12.0.1/version.txt
        # workaround for https://basecamp.com/2498935/projects/11886956/todos/465658688#comment_871862482 # noqa: E501
        shutil.copy(cfg.app.ver_file.path_deploy, bucket_ver_dir)

        # Guides
        cfg.app.s3bucket.path_latest.mkdir(exist_ok=True, parents=True)
        guides = cfg.app.bin_path / "docs/s3"
        dst = cfg.app.s3bucket.path_latest
        distutils.dir_util.copy_tree(str(guides), str(dst))
