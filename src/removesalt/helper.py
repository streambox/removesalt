import logging
import pathlib
from dataclasses import dataclass, field
from typing import List

import omegaconf
from pyscaffold import shell as pyshell

log = logging.getLogger(__name__)


# FIXME: after adding pyscaffold.shell.ShellCommand,
# now we have two ShellCommands.  Rename or remove mine.
@dataclass
class ShellCommand:
    name: str
    cmd: str
    args: List[str]
    cfg: omegaconf.DictConfig
    acceptable_returncodes: List[int] = field(default_factory=lambda: [0])


def unix2dos(path: pathlib.Path):
    cmd = ["unix2dos"]
    unix2dos = pyshell.ShellCommand(cmd)
    output = unix2dos(str(path))
    log.debug(list(output))
