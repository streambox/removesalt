import logging

import hydra
import omegaconf

from . import build_guideline, util
from .apps.iris import iris
from .apps.mediaplayer import mediaplayer
from .apps.spectra import spectra

util.initialize_resolvers()


__author__ = "Taylor Monacelli"
__copyright__ = "Taylor Monacelli"
__license__ = "MPL-2.0"


log = logging.getLogger(__name__)


# Auxiliary function
def client_call(
    build_guideline: build_guideline.BuildGuideline, cfg: omegaconf.DictConfig
):
    build_guideline.build(cfg)


@hydra.main(version_base=None, config_path="conf", config_name="config")
def my_app(cfg: omegaconf.DictConfig) -> None:
    log.info("Starting script...")

    for key in logging.Logger.manager.loggerDict:
        log.info(f"logger key: {key}")

    if cfg.app.installer_name == "spectra":
        client_call(spectra.Spectra(), cfg)
    if cfg.app.installer_name == "spectra_cdi":
        client_call(spectra.Spectra(), cfg)
    if cfg.app.installer_name == "iris":
        client_call(iris.Iris(), cfg)
    if cfg.app.installer_name == "mediaplayer":
        client_call(mediaplayer.Mediaplayer(), cfg)

    log.info("Script ends here")


if __name__ == "__main__":
    my_app()
