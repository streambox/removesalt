<!-- -*- mode: org -*-  -->
[[_TOC_]]

# 240301_WinPack
- https://basecamp.com/2498935/projects/17265419/todos/449348526#comment_921070617
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.99.0.0/win/streambox_spectra_win_1.99.0.0.zip
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.99.0.0/win/cdi/streambox_spectra_cdi_win_1.99.0.0.zip

# SCP v0.2.63
- https://basecamp.com/2498935/projects/17265419/todos/490736111#comment_920577598
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.97.0.0/win/streambox_spectra_win_1.97.0.0.zip
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.97.0.0/win/cdi/streambox_spectra_cdi_win_1.97.0.0.zip

# Add ps1
- https://basecamp.com/2498935/projects/17265419/todos/491303536#comment_920020094
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.95.0.0/win/streambox_spectra_win_1.95.0.0.zip
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.95.0.0/win/cdi/streambox_spectra_cdi_win_1.95.0.0.zip

# C07.CDI, 240217_Windows
- https://basecamp.com/2498935/projects/17265419/todos/491303536#comment_920011564
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.93.0.0/win/streambox_spectra_win_1.93.0.0.zip
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.93.0.0/win/cdi/streambox_spectra_cdi_win_1.93.0.0.zip

# HPA24-B
- https://basecamp.com/2498935/projects/17265419/todos/491303536#comment_919773868
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.92.0.0/win/streambox_spectra_win_1.92.0.0.zip
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.92.0.0/win/cdi/streambox_spectra_cdi_win_1.92.0.0.zip

# 52C05
- https://basecamp.com/2498935/projects/17265419/todos/449348526#comment_913468225
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.90.0.0/win/streambox_spectra_win_1.90.0.0.zip
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.90.0.0/win/cdi/streambox_spectra_cdi_win_1.90.0.0.zip

# 230705_AVID.zip
- https://basecamp.com/2498935/projects/17265419/todos/449348526#comment_900244113
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.88.0.0/win/streambox_spectra_win_1.88.0.0.zip
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.88.0.0/win/cdi/streambox_spectra_cdi_win_1.88.0.0.zip

# SpectraAvid25.zip
- https://basecamp.com/2498935/projects/17265419/todos/449348526#comment_897945580
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.87.0.0/win/cdi/streambox_spectra_cdi_win_1.87.0.0.zip
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.87.0.0/win/streambox_spectra_win_1.87.0.0.zip

# smp125
- https://basecamp.com/2498935/projects/11886956/todos/470508779#comment_897032999
- https://streambox-mediaplayer.s3-us-west-2.amazonaws.com/win/1.25.0.0/mediaplayer_1.25.0.0.zip

# 230530_Win.zip v.52.B17
- https://basecamp.com/2498935/projects/17265419/todos/449348526#comment_896702695
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.86.0.0/win/streambox_spectra_win_1.86.0.0.zip
- https://streambox-iris.s3-us-west-2.amazonaws.com/win/1.32.0.0/streambox_iris_win.zip

# EncoderB16E3D2 not signed
- https://basecamp.com/2498935/projects/17265419/todos/449348526#comment_896648755
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.85.0.0/win/cdi/streambox_spectra_cdi_win_1.85.0.0.zip
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.85.0.0/win/streambox_spectra_win_1.85.0.0.zip

# Mediaplayer v1.24
- https://basecamp.com/2498935/projects/11886956/todos/470508779#comment_895603629
- https://streambox-mediaplayer.s3-us-west-2.amazonaws.com/win/1.24.0.0/mediaplayer_1.24.0.0.zip

# Put iris activations to c:\programdata\streambox\iris\hash
- https://basecamp.com/2498935/projects/17971091/todos/479141117#comment_894995268
- https://streambox-iris.s3-us-west-2.amazonaws.com/win/1.31.0.0/streambox_iris_win.zip

# EncoderB16E.CDIMON.zip
- https://basecamp.com/2498935/projects/17265419/todos/449348526#comment_893736860
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.83.0.0/win/cdi/streambox_spectra_cdi_win_1.83.0.0.zip

# v.52.B16E 230428_WinDesktop
- https://basecamp.com/2498935/projects/17265419/todos/449348526#comment_893689930
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.83.0.0/win/streambox_spectra_win_1.83.0.0.zip

# v.52.B16C from 230426_Win
- https://basecamp.com/2498935/projects/17265419/todos/474218891#comment_893401832
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.82.0.0/win/streambox_spectra_win_1.82.0.0.zip

# Nest exe again like this ./streambox_spectra_cdi_win_1.82.0.0/spectra_cdi.exe
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.81.0.0/win/streambox_spectra_win_1.81.0.0.zip
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.82.0.0/win/cdi/streambox_spectra_cdi_win_1.82.0.0.zip

# B16B-CDIMON
- https://basecamp.com/2498935/projects/17265419/todos/449348526#comment_893196525
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.81.0.0/win/cdi/streambox_spectra_cdi_win_1.81.0.0.zip

# B16B-win std, StreamboxSpectra.win64.1.05.zip
- https://basecamp.com/2498935/projects/17265419/todos/449348526#comment_893186614
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.80.0.0/win/streambox_spectra_win_1.80.0.0.zip
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.80.0.0/win/cdi/streambox_spectra_cdi_win_1.80.0.0.zip

# B16LA-win.zip
- https://basecamp.com/2498935/projects/17265419/todos/478220301#comment_893062571
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.79.0.0/win/streambox_spectra_win_1.79.0.0.zip

# Move installer up one directory in zip
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.78.0.0/win/streambox_spectra_win_1.78.0.0.zip
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.78.0.0/win/cdi/streambox_spectra_cdi_win_1.78.0.0.zip

# Move Unreal files to C:\UE_5.1
- https://basecamp.com/2498935/projects/17265419/todos/478220301#comment_893034294
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.77.0.0/win/streambox_spectra_win_1.77.0.0.zip
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.77.0.0/win/cdi/streambox_spectra_cdi_win_1.77.0.0.zip

# ECP_C++_WIN32_0.2.57.zip
- https://basecamp.com/2498935/projects/17265419/todos/478247644#comment_892982545
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.76.0.0/win/streambox_spectra_win_1.76.0.0.zip
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.76.0.0/win/cdi/streambox_spectria_cdi_win_1.76.0.0.zip

# ECP_CPP_WIN32_x86_0.2.56.zip
- https://basecamp.com/2498935/projects/17265419/todos/477305398#comment_892981188
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.75.0.0/win/streambox_spectra_win_1.75.0.0.zip
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.75.0.0/win/cdi/streambox_spectria_cdi_win_1.75.0.0.zip

# B16-CDI.MON
- https://basecamp.com/2498935/projects/17265419/todos/449348526#comment_892979702
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.74.0.0/win/streambox_spectra_win_1.74.0.0.zip
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.74.0.0/win/cdi/streambox_spectria_cdi_win_1.74.0.0.zip

# ECP_C++_WIN32_0.2.55.zip
- https://basecamp.com/2498935/projects/17265419/todos/477305398#comment_892958030
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.73.0.0/win/streambox_spectra_win_1.73.0.0.zip
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.73.0.0/win/cdi/streambox_spectria_cdi_win_1.73.0.0.zip

# Add unreal as zip
- https://basecamp.com/2498935/projects/17265419/todos/478220301#comment_892900662
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.72.0.0/win/streambox_spectra_win_1.72.0.0.zip
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.72.0.0/win/cdi/streambox_spectra_cdi_win_1.72.0.0.zip

# OFX plugin v.25.01
- https://basecamp.com/2498935/projects/17265419/todos/449348526#comment_892636980
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.71.0.0/win/streambox_spectra_win_1.71.0.0.zip
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.71.0.0/win/cdi/streambox_spectra_cdi_win_1.71.0.0.zip

# 230419_EncDecDesktop
- https://basecamp.com/2498935/projects/17265419/todos/449348526#comment_892636229
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.70.0.0/win/streambox_spectra_win_1.70.0.0.zip
- https://streambox-iris.s3-us-west-2.amazonaws.com/win/1.30.0.0/streambox_iris_win.zip

# 5215L2-CDI
- https://basecamp.com/2498935/projects/17265419/todos/476513745#comment_892453344
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.69.5.0/win/cdi/streambox_spectra_cdi_win.zip

# 52.b15L3-std
- https://basecamp.com/2498935/projects/17265419/todos/449348526#comment_891660830
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.67.0.0/win/streambox_spectra_win_1.67.0.0.zip

# SCP 0.2.55
- https://basecamp.com/2498935/projects/17265419/todos/474218891#comment_891660200
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.66.0.0/win/streambox_spectra_win_cdi_1.66.0.0.zip

# Add powershell dual scripts
- https://basecamp.com/2498935/projects/17265419/todos/477305398#comment_891583352
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.64.0.0/win/streambox_spectra_win_1.64.0.0.zip
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.65.0.0/win/streambox_spectra_win_cdi_1.65.0.0.zip

# SCP 0.2.54
- https://basecamp.com/2498935/projects/17265419/todos/477305398#comment_891564354
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.63.0.0/win/streambox_spectra_win_1.63.0.0.zip

# Adobe.v.28.0 with 52.B15L standard
- https://basecamp.com/2498935/projects/17265419/todos/449348526#comment_891198932
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.62.0.0/win/streambox_spectra_win_1.62.0.0.zip

# Adobe.v.28.0 with 5215L2-CDI.zip: This is bad, don't use.  Installer bundled wrong exe.  Use 5215L2-CDI instead.
- https://basecamp.com/2498935/projects/17265419/todos/449348526#comment_891198932
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.61.0.0/win/streambox_spectra_win_cdi_1.61.0.0.zip

# 5215L2-CDI.zip
- https://basecamp.com/2498935/projects/17265419/todos/449348526#comment_890950466
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.60.0.0/win/streambox_spectra_win_cdi_1.60.0.0.zip

# 52.b15L-CDI reposted with encoder3 included
- https://basecamp.com/2498935/projects/17265419/todos/449348526#comment_890832300
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.59.0.0/win/streambox_spectra_win_cdi_1.59.0.0.zip

# 52.b15L-CDI missing encoder3
- https://basecamp.com/2498935/projects/17265419/todos/449348526#comment_890832300
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.58.0.0/win/streambox_spectra_win_cdi_1.58.0.0.zip

# 52.B15L
- https://basecamp.com/2498935/projects/17265419/todos/449348526#comment_890806773
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.57.0.0/win/streambox_spectra_win_1.57.0.0.zip

# Installer: remove dual setup, its too early
- https://basecamp.com/2498935/projects/17265419/todos/466463613#comment_890324822
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.55.1.0/win/streambox_spectra_win_1.55.1.0.zip

# ECP_C++_WIN32_0.2.51.zip
- https://basecamp.com/2498935/projects/17265419/todos/466463613#comment_890324822
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.55.0.0/win/streambox_spectra_win_1.55.0.0.zip

# 230321_EncDecDesktop
- https://basecamp.com/2498935/projects/17265419/todos/449348526#comment_889780161
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.54.0.0/win/streambox_spectra_win_1.54.0.0.zip
- https://streambox-iris.s3-us-west-2.amazonaws.com/win/1.29.0.0/streambox_iris_win.zip

# 230317_EncDecAdobe
- https://basecamp.com/2498935/projects/17265419/todos/449348526#comment_889485488
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.53.0.0/win/streambox_spectra_win_1.53.0.0.zip
- https://streambox-iris.s3-us-west-2.amazonaws.com/win/1.28.0.0/streambox_iris_win.zip

# Encoder.52.15D.CDI.zip
- https://basecamp.com/2498935/projects/17265419/todos/449348526#comment_889309109
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.52.0.0/win/streambox_spectra_win_cdi_1.52.0.0.zip

# Encoder.52.15D.zip
- https://basecamp.com/2498935/projects/17265419/todos/449348526#comment_889309109
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.51.0.0/win/streambox_spectra_win_1.51.0.0.zip

# IrisInstaller - revert nssm
- https://basecamp.com/2498935/projects/17971091/todos/475958377#comment_889277502
- https://streambox-iris.s3-us-west-2.amazonaws.com/win/1.27.0.0/streambox_iris_win.zip

# SCP v0.2.48
- https://basecamp.com/2498935/projects/17265419/todos/475958769#comment_888725638
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.50.0.0/win/streambox_spectra_win_1.50.0.0.zip

# SCP v0.2.47
- https://basecamp.com/2498935/projects/17265419/todos/475958769#comment_888712464
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.49.0.0/win/streambox_spectra_win_1.49.0.0.zip

# 230309_WinDesktop: Encoder v3.202.52.B15C, OFX v.24.20
- https://basecamp.com/2498935/projects/17265419/todos/449348526#comment_888607926
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.48.0.0/win/streambox_spectra_win_1.48.0.0.zip

# Signed with new cert from 3/8/2023
- https://basecamp.com/2498935/projects/4658296/todos/476150362#comment_888580885
- https://streambox-mediaplayer.s3-us-west-2.amazonaws.com/win/1.23.1.0/mediaplayer.zip
- https://streambox-iris.s3-us-west-2.amazonaws.com/win/1.26.1.0/streambox_iris_win.zip
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.47.1.0/win/streambox_spectra_win_1.47.1.0.zip

# ICP v0.2.46
- https://basecamp.com/2498935/projects/17971091/todos/475558029#comment_888297465
- https://streambox-iris.s3-us-west-2.amazonaws.com/win/1.26.0.0/streambox_iris_win.zip

# Encoder 52.B15B
- https://basecamp.com/2498935/projects/4645512/todos/474123993#comment_888324318
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.47.0.0/win/streambox_spectra_win_1.47.0.0.zip

# ICP v0.2.45
- https://basecamp.com/2498935/projects/17971091/todos/475558029#comment_888297465
- https://streambox-iris.s3-us-west-2.amazonaws.com/win/1.25.0.0/streambox_iris_win.zip

# Encoder B15A4
- https://basecamp.com/2498935/projects/17265419/todos/449348526#comment_888289050
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.46.0.0/win/streambox_spectra_win_1.46.0.0.zip

# Encoder B15ACDI
- https://basecamp.com/2498935/projects/17265419/todos/474218891#comment_888228489
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.45.0.0/win/streambox_spectra_win_1.45.0.0.zip

# 230306_WinDesktop
- https://basecamp.com/2498935/projects/17265419/todos/449348526#comment_888189053
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.44.0.0/win/streambox_spectra_win_1.44.0.0.zip

# Encoder B15A2
- https://basecamp.com/2498935/projects/17265419/todos/449348526#comment_888155259
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.43.0.0/win/streambox_spectra_win_1.43.0.0.zip

# Encoder 52.B15-AWS-CDI
- https://basecamp.com/2498935/projects/17265419/todos/475058127#comment_888069490
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.42.0.0/win/streambox_spectra_win_1.42.0.0.zip

# 230303_WinDesktop
- https://basecamp.com/2498935/projects/17265419/todos/449348526#comment_888036942
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.41.0.0/win/streambox_spectra_win_1.41.0.0.zip

# SCP v0.2.44
- https://basecamp.com/2498935/projects/17265419/todos/475567529#comment_887398118
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.40.0.0/win/streambox_spectra_win_1.40.0.0.zip

# ICP v0.2.44
- https://basecamp.com/2498935/projects/17971091/todos/475558029#comment_887398469
- https://streambox-iris.s3-us-west-2.amazonaws.com/win/1.24.0.0/streambox_iris_win.zip

# SCP v0.2.43
- https://basecamp.com/2498935/projects/17265419/todos/474218891#comment_887381893
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.39.0.0/win/streambox_spectra_win_1.39.0.0.zip

# Promote ntv beta adobe spectra plugin to master
- https://basecamp.com/2498935/projects/17265419/todos/475368918#comment_887373276
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.38.0.0/win/streambox_spectra_win_1.38.0.0.zip

# Adobe .PRM update for ntv beta test
- https://basecamp.com/2498935/projects/17265419/todos/475368918#comment_886957874
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.37.0.0/win/streambox_spectra_win_1.37.0.0.zip

# SCP v0.2.42
- https://basecamp.com/2498935/projects/17265419/todos/474218891#comment_886917563
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.36.0.0/win/streambox_spectra_win_1.36.0.0.zip
- https://streambox-iris.s3-us-west-2.amazonaws.com/win/1.23.0.0/streambox_iris_win.zip

# ICP v0.2.42
This is bad, don't use.  See [here](https://basecamp.com/2498935/projects/17265419/todos/474218891#comment_886843877)
- https://basecamp.com/2498935/projects/17265419/todos/474218891#comment_886785167
- https://streambox-iris.s3-us-west-2.amazonaws.com/win/1.22.0.0/streambox_iris_win.zip

# Encoder v.52.B14D2
- https://basecamp.com/2498935/projects/17265419/todos/475058127#comment_886374145
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.35.0.0/win/streambox_spectra_win_1.35.0.0.zip

# SCP v0.2.40
- https://basecamp.com/2498935/projects/17265419/todos/474218891#comment_886268602
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.34.0.0/win/streambox_spectra_win_1.34.0.0.zip

# Encoder-52B14C2(NTV) v.52.B14C2
- https://basecamp.com/2498935/projects/18566065/todos/474470423#comment_886177495
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.33.0.0/win/streambox_spectra_win_1.33.0.0.zip

# SCP v0.2.39
- https://basecamp.com/2498935/projects/18566065/todos/474470423#comment_886128311
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.32.0.0/win/streambox_spectra_win_1.32.0.0.zip

# SCP v0.2.38
- https://basecamp.com/2498935/projects/18566065/todos/474470423#comment_886107414
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.31.0.0/win/streambox_spectra_win_1.31.0.0.zip

# SpectraInstaller - encoder preference 3 or 5
- https://basecamp.com/2498935/projects/18566065/todos/474470423#comment_885803711
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.30.0.0/win/streambox_spectra_win_1.30.0.0.zip

# SpectraInstaller icon update
- https://basecamp.com/2498935/projects/18566065/todos/474470423#comment_885680919
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.29.0.0/win/streambox_spectra_win_1.29.0.0.zip

# SCP v0.2.37
- https://basecamp.com/2498935/projects/18566065/todos/474470423#comment_885645579
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.28.0.0/win/streambox_spectra_win_1.28.0.0.zip
- https://streambox-iris.s3-us-west-2.amazonaws.com/win/1.21.0.0/streambox_iris_win.zip

# ICP v0.2.37
This is bad, don't use.  See [here](https://basecamp.com/2498935/projects/18566065/todos/474470423#comment_885670427)
- https://basecamp.com/2498935/projects/18566065/todos/474470423#comment_885645579
- https://streambox-iris.s3-us-west-2.amazonaws.com/win/1.20.0.0/streambox_iris_win.zip

# Reverted nssm
- https://basecamp.com/2498935/projects/18566065/todos/474470423#comment_885505691
- https://streambox-iris.s3-us-west-2.amazonaws.com/win/1.19.0.0/streambox_iris_win.zip
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.27.0.0/win/streambox_spectra_win_1.27.0.0.zip

# SCP v2.35
- https://basecamp.com/2498935/projects/18566065/todos/474470423#comment_885503522
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.26.0.0/win/streambox_spectra_win_1.26.0.0.zip

# nssm 2.24 from (2014-08-31)
- https://streambox-iris.s3-us-west-2.amazonaws.com/win/1.19.0.0/streambox_iris_win.zip
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.25.0.0/win/streambox_spectra_win_1.25.0.0.zip

# 230206_EncDec v.52.B14
- https://basecamp.com/2498935/projects/17265419/todos/449348526#comment_885224163
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.24.0.0/win/streambox_spectra_win_1.24.0.0.zip
- https://streambox-iris.s3-us-west-2.amazonaws.com/win/1.18.0.0/streambox_iris_win.zip

# 230206_OFX_Win v.24.18
- https://basecamp.com/2498935/projects/17265419/todos/449348526#comment_885224163
- https://gitlab.com/streambox/streambox_spectra_win_assets/-/blob/master/docs/nocuda-test.md
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.23.0.0/win/streambox_spectra_win_1.23.0.0.zip

# IrisInstaller
- https://basecamp.com/2498935/projects/17971091/todos/444122740#comment_885185325
- https://streambox-iris.s3-us-west-2.amazonaws.com/win/1.17.0.0/streambox_iris_win.zip

# ICP v0.2.34
- https://basecamp.com/2498935/projects/17971091/todos/444122740#comment_885032915
- https://streambox-iris.s3-us-west-2.amazonaws.com/win/1.16.0.0/streambox_iris_win.zip

# Decoder v.52.B13C
- https://basecamp.com/2498935/projects/17971091/todos/444122740#comment_884760181
- https://streambox-iris.s3-us-west-2.amazonaws.com/win/1.15.0.0/streambox_iris_win.zip

# 230127_EncWin v.52.B13B
- https://basecamp.com/2498935/projects/18566065/todos/473656782#comment_884282691
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.22.0.0/win/streambox_spectra_win_1.22.0.0.zip

# 230112_EncDec v.52.B12D
- https://basecamp.com/2498935/projects/17265419/todos/449348526#comment_882889626
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.21.0.0/win/streambox_spectra_win_1.21.0.0.zip
- https://streambox-iris.s3-us-west-2.amazonaws.com/win/1.14.0.0/streambox_iris_win.zip

# Mediaplayer
- https://basecamp.com/2498935/projects/11886956/todos/470508779#comment_881582041
- https://streambox-mediaplayer.s3-us-west-2.amazonaws.com/win/1.23.0.0/mediaplayer.zip

# 221227_Iris v.52.B12A
- https://basecamp.com/2498935/projects/17265419/todos/449348526#comment_881509915
- https://streambox-iris.s3-us-west-2.amazonaws.com/win/1.13.0.0/streambox_iris_win.zip

# 221226_EncDec	v.52.B12
- https://basecamp.com/2498935/projects/17265419/todos/449348526#comment_881481038
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.20.0.0/win/streambox_spectra_win_1.20.0.0.zip
- https://streambox-iris.s3-us-west-2.amazonaws.com/win/1.12.0.0/streambox_iris_win.zip

# 221207_Adobe, 221207_Encoder, v.52.B11A and Adobe v.25.0
- https://basecamp.com/2498935/projects/17265419/todos/449348526#comment_879706848
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.19.0.0/win/streambox_spectra_win_1.19.0.0.zip

# SCP 2.33
- https://basecamp.com/2498935/projects/17265419/todos/471623412#comment_879622107
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.18.0.0/win/streambox_spectra_win_1.18.0.0.zip

# 221130_EncDec/v.52.B11
- https://basecamp.com/2498935/projects/17265419/todos/449348526#comment_879093093
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.17.0.0/win/streambox_spectra_win_1.17.0.0.zip
- https://streambox-iris.s3-us-west-2.amazonaws.com/win/1.11.0.0/streambox_iris_win.zip

# SCP 2.32
- https://basecamp.com/2498935/projects/13515285/todos/462509667#comment_877859753
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.16.0.0/win/streambox_spectra_win_1.16.0.0.zip

# SpectraPlugin.ofx
- https://basecamp.com/2498935/projects/17265419/todos/470267935#comment_876874632
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.15.1.0/win/streambox_spectra_win_1.15.1.0.zip

# SpectraPlugin.ofx
- https://basecamp.com/2498935/projects/17265419/todos/470267935#comment_876701964
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.15.0.0/win/streambox_spectra_win_1.15.0.0.zip

# Mediaplayer
- https://basecamp.com/2498935/projects/11886956/todos/468145135#comment_876303219
- https://streambox-mediaplayer.s3-us-west-2.amazonaws.com/win/1.22.0.0/mediaplayer.zip

# 221031_EncDec/v.52.B09
- https://basecamp.com/2498935/projects/17265419/todos/449348526#comment_875821258
- https://streambox-iris.s3-us-west-2.amazonaws.com/win/1.10.0.0/streambox_iris_win.zip
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.14.0.0/win/streambox_spectra_win_1.14.0.0.zip

# 221010_EncDec/v.52.B08
- https://basecamp.com/2498935/projects/17265419/todos/449348526#comment_874157960
- https://streambox-iris.s3-us-west-2.amazonaws.com/win/1.9.0.0/streambox_iris_win.zip
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.13.0.0/win/streambox_spectra_win_1.13.0.0.zip

# 221005_EncDec/v.52.B07
- https://basecamp.com/2498935/projects/17265419/todos/449348526#comment_873108957
- https://streambox-iris.s3-us-west-2.amazonaws.com/win/1.8.0.0/streambox_iris_win.zip
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.12.0.0/win/streambox_spectra_win_1.12.0.0.zip

# Spectra
- https://basecamp.com/2498935/projects/17265419/todos/461780089#comment_872348243
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.11.0.0/win/streambox_spectra_win_1.11.0.0.zip

# Mediaplayer
- https://basecamp.com/2498935/projects/11886956/todos/468145135#comment_872228848
- https://streambox-mediaplayer.s3-us-west-2.amazonaws.com/win/1.21.0.0/mediaplayer.zip

# Spectra
- https://basecamp.com/2498935/projects/17265419/todos/461780089#comment_869860284
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.10.3.0/win/streambox_spectra_win_1.10.3.0.zip

# Mediaplayer
- https://basecamp.com/2498935/projects/11886956/todos/465658688#comment_869742836
- https://streambox-mediaplayer.s3-us-west-2.amazonaws.com/win/1.20.0.0/mediaplayer.zip

# 220902_EncDec
- https://basecamp.com/2498935/projects/17265419/todos/449348526#comment_869687002
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.10.0.0/win/streambox_spectra_win_1.10.0.0.zip
- https://streambox-iris.s3-us-west-2.amazonaws.com/win/1.7.0.0/streambox_iris_win.zip

# 220829_Ofx
- https://basecamp.com/2498935/projects/17265419/todos/449348526#comment_869078089
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.9.0.0/win/streambox_spectra_win_1.9.0.0.zip

# 220824_FullExe/v3.202.52.B01
- https://basecamp.com/2498935/projects/17265419/todos/449348526#comment_868638040
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.8.0.0/win/streambox_spectra_win_1.8.0.0.zip
- https://streambox-iris.s3-us-west-2.amazonaws.com/win/1.6.0.0/streambox_iris_win.zip

# 220822_EncDec/v3.202.52.A36D
- https://basecamp.com/2498935/projects/17265419/todos/449348526#comment_868327566
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.7.0.0/win/streambox_spectra_win_1.7.0.0.zip
- https://streambox-iris.s3-us-west-2.amazonaws.com/win/1.5.0.0/streambox_iris_win.zip

# Mediaplayer
- https://basecamp.com/2498935/projects/11886956/todos/465658688#comment_868219015
- https://streambox-mediaplayer.s3-us-west-2.amazonaws.com/win/1.19.0.1/mediaplayer.zip

# 220819_EncDec/v3.202.52.A36C
- bad, don't use, see https://basecamp.com/2498935/projects/17265419/todos/449348526#comment_868327566
- https://basecamp.com/2498935/projects/17265419/todos/449348526#comment_868182045
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.6.0.0/win/streambox_spectra_win_1.6.0.0.zip
- https://streambox-iris.s3-us-west-2.amazonaws.com/win/1.4.0.0/streambox_iris_win.zip

# Mediaplayer
- https://basecamp.com/2498935/projects/11886956/todos/465658688#comment_867570750
- https://streambox-mediaplayer.s3-us-west-2.amazonaws.com/win/1.18.0.0/mediaplayer_all_1.18.0.0.zip

# Spectra.prm
- https://basecamp.com/2498935/projects/17265419/todos/466337145#comment_867940574
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.5.0.0/win/streambox_spectra_win_1.5.0.0.zip

# 220810_EncWin/v3.202.52.A35E
- https://basecamp.com/2498935/projects/17265419/todos/449348526#comment_867196669
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.4.0.0/win/streambox_spectra_win_1.4.0.0.zip

# 220802_EncEx/v3.202.52.A35D
- https://basecamp.com/2498935/projects/17265419/todos/449348526#comment_866374463
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.3.0.0/win/streambox_spectra_win_1.3.0.0.zip
- https://streambox-iris.s3-us-west-2.amazonaws.com/win/1.3.0.0/streambox_iris_win.zip

# 220801_EncEx/v3.202.52.A35C
- https://basecamp.com/2498935/projects/17265419/todos/463625652#comment_866196850
- https://basecamp.com/2498935/projects/17265419/todos/449348526#comment_866196645
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.2.0.0/win/streambox_spectra_win_1.2.0.0.zip
- https://streambox-iris.s3-us-west-2.amazonaws.com/win/1.2.0.0/streambox_iris_win.zip

# 220801_EncEx/v3.202.52.A35B
- https://basecamp.com/2498935/projects/17265419/todos/463625652#comment_866180954
- https://streambox-spectra.s3-us-west-2.amazonaws.com/1.1.6.2/win/streambox_spectra_win_1.1.6.2.zip
- https://streambox-iris.s3-us-west-2.amazonaws.com/win/1.1.1.1/streambox_iris_win.zip
