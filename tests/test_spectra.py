# flake8: noqa
import pathlib

import pytest

avid_directories = {
    r"C:\Program Files\Avid\AVX2_Plug-ins",
}
avid_files = {
    r"C:\Program Files\Avid\AVX2_Plug-ins\Spectra.acf",
}
avid_files_and_directories = avid_directories | avid_files


streambox_directories = {
    r"C:\Program Files\Streambox\Spectra",
    r"C:\Program Files\Streambox\Spectra\dist",
    r"C:\ProgramData\Streambox\SpectraUI",
    r"C:\ProgramData\Streambox\SpectraUI\hash",
}
streambox_files = {
    r"C:\Program Files\Streambox\Spectra\control.ps1",
    r"C:\Program Files\Streambox\Spectra\control.psm1",
    r"C:\Program Files\Streambox\Spectra\disable-blackmagic.ps1",
    r"C:\Program Files\Streambox\Spectra\Encoder3.exe",
    r"C:\Program Files\Streambox\Spectra\Encoder5.exe",
    r"C:\Program Files\Streambox\Spectra\nssm.exe",
    r"C:\Program Files\Streambox\Spectra\Processing.NDI.Lib.x64.dll",
    r"C:\Program Files\Streambox\Spectra\sbxcmd.exe",
    r"C:\Program Files\Streambox\Spectra\service-error.log",
    r"C:\Program Files\Streambox\Spectra\service.log",
    r"C:\Program Files\Streambox\Spectra\dist\chromactivate.exe",
    r"C:\Program Files\Streambox\Spectra\dist\encassist.ini",
    r"C:\Program Files\Streambox\Spectra\dist\set_defaults.exe",
    r"C:\Program Files\Streambox\Spectra\dist\SpectraControlPanel.exe",
    r"C:\ProgramData\Streambox\SpectraUI\liveServers.xml",
    r"C:\ProgramData\Streambox\SpectraUI\settings.xml",
    r"C:\ProgramData\Streambox\SpectraUI\settings.xml.bak",
    r"C:\ProgramData\Streambox\SpectraUI\version.txt",
    r"C:\ProgramData\Microsoft\Windows\Start Menu\Programs\Streambox\Spectra\Spectra Control Panel.lnk",
}
streambox_files_and_directories = streambox_directories | streambox_files

adobe_directories = {
    r"C:\Program Files\Adobe\Common",
    r"C:\Program Files\Adobe\Common\Plug-ins",
    r"C:\Program Files\Adobe\Common\Plug-ins\7.0",
    r"C:\Program Files\Adobe\Common\Plug-ins\7.0\MediaCore",
}
adobe_files = {
    r"C:\Program Files\Adobe\Common\Plug-ins\7.0\MediaCore\Spectra.prm",
}
adobe_files_and_direcotries = adobe_directories | adobe_files

ofx_plugin_direcotries = {
    r"C:\Program Files\Common Files\OFX\Plugins\SpectraPlugin.ofx.bundle\Contents\Resources",
    r"C:\Program Files\Common Files\OFX\Plugins\SpectraPlugin.ofx.bundle\Contents\Win64",
}
ofx_plugin_files = {
    r"C:\Program Files\Common Files\OFX\Plugins\SpectraPlugin.ofx.bundle\Contents\Resources\com.Streambox.Spectra.png",
    r"C:\Program Files\Common Files\OFX\Plugins\SpectraPlugin.ofx.bundle\Contents\Win64\SpectraPlugin.ofx",
}
ofx_plugin_files_and_direcotries = ofx_plugin_direcotries | ofx_plugin_files

empty_files = {
    r"C:\Program Files\Streambox\Spectra\service-error.log",
    r"C:\Program Files\Streambox\Spectra\service.log",
}

all_files = adobe_files | avid_files | empty_files | ofx_plugin_files | streambox_files

all_directories = (
    adobe_directories
    | avid_directories
    | ofx_plugin_direcotries
    | streambox_directories
)

all_files_and_directories = all_files | all_directories


@pytest.mark.parametrize("path_str", all_files_and_directories)
def test_path_exists(path_str):
    assert pathlib.Path(path_str).exists()


@pytest.mark.parametrize("path_str", all_directories)
def test_path_exists(path_str):
    assert pathlib.Path(path_str).is_dir()


@pytest.mark.parametrize("path_str", all_files)
def test_paths_are_files(path_str):
    assert pathlib.Path(path_str).is_file()


@pytest.mark.parametrize("path_str", all_files - empty_files)
def test_files_are_not_empty(path_str):
    path = pathlib.Path(path_str)
    assert path.stat().st_size


@pytest.mark.parametrize("path_str", empty_files)
def test_files_are_empty(path_str):
    path = pathlib.Path(path_str)
    assert path.stat().st_size == 0
