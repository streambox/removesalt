$commands = @(
    "goreleaser"
    "ytt"
    "dos2unix"
)

foreach ($command in $commands) {
    $commandInfo = Get-Command -Name $command -ErrorAction SilentlyContinue

    if ($null -eq $commandInfo) {
        Write-Host "$command not found. Installing with Chocolatey..."
        choco install --yes --no-progress $command
    } else {
        Write-Host "$command is already installed."
    }
}

python -mvenv build\.venv
. build\.venv\Scripts\activate.ps1
pip install --disable-pip-version-check wheel --quiet --use-pep517 --editable .[testing]
pip --disable-pip-version-check list
$wix_dir = (Get-ChildItem -Recurse C:\Program*\Wix*Toolset*\bin -Filter "heat.exe" | select-object -first 1).Directory.FullName
$env:PATH = "$wix_dir;$env:PATH"
